let checkboxbutton = document.querySelector(".rd");
let todoInput = document.querySelector(".tb");
let todoList = document.querySelector(".todo-container");
let buttonsContainer = document.querySelector(".buttons");
let allTodos = document.querySelector(".all");
let activeTodos = document.querySelector(".active");
let completedTodos = document.querySelector(".completed");
let clearCompletedTodos = document.querySelector(".clear");
let totalTodos = 0;
function createTodoItem(event) {
  if (event.type === "keyup" && event.key === "Enter") {
    let content = todoInput.value;
    if (content !== "") {
      let todoItem = document.createElement("div");
      todoItem.classList.add("todo-item");
      let checkbox = document.createElement("input");
      checkbox.setAttribute("type", "checkbox");
      checkbox.checked = false;
      let todoContent = document.createElement("span");
      todoContent.textContent = content;
      todoContent.classList.add("todo-content");
      let removeButton = document.createElement("button");
      removeButton.textContent = "✕";
      removeButton.classList.add("cross-button");
      todoItem.appendChild(checkbox);
      todoItem.appendChild(todoContent);
      todoItem.appendChild(removeButton);
      todoList.insertBefore(todoItem, buttonsContainer);
      totalTodos++;
      updateTotalTodosCount();
      todoInput.value = "";
      checkboxbutton.checked = false;
    }
  }
}
function updateTotalTodosCount() {
  let totalTodosElement = document.querySelector(".total-todos");
  totalTodosElement.textContent = totalTodos + " items left";
}
function showAllTodos() {
  let todos = todoList.querySelectorAll(".todo-item");
  todos.forEach((item) => {
    item.style.display = "flex";
  });
}
function showActiveTodos() {
  let todos = todoList.querySelectorAll(".todo-item");
  todos.forEach((item) => {
    let checkbox = item.querySelector("input[type='checkbox']");
    if (checkbox.checked === false) {
      item.style.display = "flex";
    } else {
      item.style.display = "none";
    }
  });
}
function showCompletedTodos() {
  let todos = todoList.querySelectorAll(".todo-item");
  todos.forEach((item) => {
    let checkbox = item.querySelector("input[type='checkbox']");
    if (checkbox.checked !== false) {
      item.style.display = "flex";
    } else {
      item.style.display = "none";
    }
  });
}
function toClearCompletedTodos() {
  let todos = Array.from(todoList.querySelectorAll(".todo-item"));
  let completedTodos = todos.filter((item) => {
    let checkbox = item.querySelector("input[type='checkbox']");
    if (checkbox.checked !== false) {
      return item;
    }
  });
  completedTodos.forEach((item) => {
    todoList.removeChild(item);
  });
  totalTodos = totalTodos - completedTodos.length;
  updateTotalTodosCount();
}
todoInput.addEventListener("keyup", createTodoItem);
buttonsContainer.addEventListener("click", (e) => {
  if (e.target.id === "all") {
    showAllTodos();
  }
  if (e.target.id === "active") {
    showActiveTodos();
  }
  if (e.target.id === "completed") {
    showCompletedTodos();
  }
  if (e.target.id === "clear") {
    toClearCompletedTodos();
  }
});
todoList.addEventListener("change", (e) => {
  if (e.target.type === "checkbox") {
    let todoContent = e.target.nextElementSibling;
    if (e.target.checked) {
      todoContent.style.textDecoration = "line-through";
    } else {
      todoContent.style.textDecoration = "none";
    }
  }
});

todoList.addEventListener("click", (e) => {
  if (e.target.classList.contains("cross-button")) {
    let todoItem = e.target.parentNode;
    todoList.removeChild(todoItem);
    totalTodos--;
    updateTotalTodosCount();
  }
});
